#+TITLE: About - James Richardson's Engineering Notebook
#+AUTHOR: James Richardson
#+EMAIL: j@mesrichardson.com
#+DATE:
#+STARTUP: showall
#+STARTUP: inlineimages

#+options: title:nil

Nothing here yet.

See [[https://jamestechnotes.com/blog/blog.html][blog posts]] or [[https://jamesrichardson.name][my personal site]].
